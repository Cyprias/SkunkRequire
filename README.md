# SkunkRequire.js, Copyright 2015	cyprias@gmail.com

## Synopsis

Simple CommonJS 1.0 like implementation of require() for Skunkworks.

## Functionality
- Creates a global require() function for scripts to load modules into memory.

## API
`<Required> [optional]`

- `var module         = require(<name/path>);`: Load a module.


## Example usage.

```
	// Load SkunkRequire via your swx file.
    <script src="modules\\SkunkRequire\\SkunkRequire.js"/>
```
```
	// Create a module.
	exports.foo = function() {
		return "bar";
	};

	// Or Create a module that's compatible with both swx and require().
	(function (factory) {
		if (typeof module === 'object' && module.exports) {
			module.exports = factory(); // Loaded via require(), returns as exports.
		} else {
			LibFooBar = factory();// Loaded via swx, Global object for your module.
		}
	}(function () {// Where all your code goes.
		function foo() {
			return "bar";
		}
		return { // Public API
			foo    : foo,
		};
	}));
```
```
    // To load a module.
    var LibFooBar = require("modules\\LibFooBar.js");
```

## License
MIT License	(http://opensource.org/licenses/MIT)
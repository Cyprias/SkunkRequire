/*
	Series of tests to make sure require() is working properly.
*/

function consoleLog() { // 'console' is taken by Skunkworks.
	var args = Array.prototype.slice.call(arguments);
	skapi.OutputLine("[RequireTests] " + args.join('\t'), opmConsole);
};

var tests = [];

tests.push({name:"adjacent script", method:function() {
	var value = require("adjacent_script");
	return (value == "adjacent_script.js");
}});

tests.push({name:"subfolder script", method:function() {
	var value = require("subfolder\\script"); // test_env\tests\subfolder\script.js
	return (value == "subfolder script.js");
}});

tests.push({name:"subfolder script 2", method:function() {
	var value = require("subfolder\\subfolder2\\script.js"); // test_env\tests\subfolder\subfolder2\script.js
	return (value == "subfolder\subfolder2\script.js");
}});

tests.push({name:"adjacent folder test", method:function() {
	var value = require("..\\adjacent_folder\\script.js");// test_env\adjacent_folder\script.js
	return (value == "adjacent folder test");
}});

tests.push({name:"module test", method:function() {
	var value = require("test_module");// test_env\tests\modules\test_module.js
	return (value == "module test");
}});

tests.push({name:"module test", method:function() {
	var value = require("module requiring module");// test_env\tests\modules\module requiring module.js
	return (value == "module test");
}});

tests.push({name:"Module Folder test", method:function() {
	var value = require("ModuleFolder"); // test_env\tests\modules\ModuleFolder\ModuleFolder.js
	return (value == "Module Folder test");
}});

tests.push({name:"adjacent module test", method:function() {
	var value = require("..\\adjacent_folder\\adjacent script with modules.js"); // test_env\adjacent_folder\adjacent script with modules.js
	return (value == "adjacent module test");
}});

function runTests() {
	var t;
	var pass;
	var passCount = 0;
	for (var i=0; i<tests.length; i++) {
		t = tests[i];
		pass = t.method();
		consoleLog(t.name, pass);
		if (pass) passCount++;
	}
	consoleLog("Success rate: " + ((passCount / tests.length) *100)+ "%");
};

function main() {
	runTests();
	console.StopScript();
};
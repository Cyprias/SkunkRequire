/********************************************************************************************\
	File Name:      SkunkRequire.js
	Purpose:        Simple CommonJS like implementation of require() for Skunkworks.
	Creator:        Cyprias
	Date:           04/01/2015
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/)
\*********************************************************************************************/

/* eslint-disable */
var MAJOR = "SkunkRequire-1.0";
var MINOR = 200106; // Year Month Day
/* eslint-enable */

/**
 * Evals a module file and returns its exports.
 *
 * @param {string} path The name or path to the file we want to load.
 * @param {object} params: Collection of arguments.
 * @param {object} params.parent: Module loading another module. Usually added automatically.
 * @param {string} params.dirnameOverride A directory path to set as the __dirname instead of the file's actually file path.
 * @param {boolean} params.silent Don't throw file not found error.
 * @return {object} The module exports.
 */
require = (function() {
	var debugging = false;
	function outputLine() {
		skapi.OutputLine("[SkunkRequire.js] " + Array.prototype.slice.call(arguments).join('\t'), opmConsole);
	}
	
	function debug() {
		if (debugging == true) {
			outputLine.apply(this, arguments);
		}
	}
	
	
	// memoized export objects
	var exportsObjects = {};
 
	var FileReadTypes = {
		adTypeBinary : 1,
		adTypeText   : 2
	};
	
	function isNumeric(n) {
		return !isNaN(parseFloat(n)) && isFinite(n);
	}

	// Traverse the filesystem to find a module file and return file path.
	function findModule(params) {
		var name = params.name;
		var parent = params.parent;
		debug("<findModule> ", name);

		var fso = new ActiveXObject("Scripting.FileSystemObject");
		
		// Check if full path exists first.
		if (fso.FileExists(name)) {
			return fso.GetFile(name).Path;
		}
		
		// filename without the path.
		var fileName = name;
		if (name.match(/(.*)\\(.*)$/)) {
			fileName = RegExp.$2;
		}
		debug(" fileName: ", fileName);

		// name without the file type.
		var shortName = fileName;
		var fileType;
		if (name.charAt(0) != "." && name.match(/(.*)\.(.*)$/)) {
			shortName = RegExp.$1;
			fileType = RegExp.$2;
		}
		debug(" name: ", name);
		debug(" shortName: ", shortName);
		debug(" fileType: ", fileType + ", isNumeric: " + isNumeric(fileType));

		// Check if fileType is a number, might just be a version number in the name. (Script-1.0)
		if (fileType && isNumeric(fileType)) {
			shortName += fileType;
			fileType = undefined;
		}
		
		var roots = [];

		// Use specified dirname as our root first.
		if (params.dirnameOverride) {
			roots.push(params.dirnameOverride + "\\");
		}
		
		// Get our reletive path.
		if (parent) {
			debug("parent: ", parent);
			var parentFileName = parent.filename;
			var parentFile = fso.GetFile(parentFileName);
			var parentFolder = parentFile.ParentFolder;
			var parentFolderPath = parentFolder.Path;
			roots.push(parentFolderPath + "\\");
		}

		// Add the path the swx is running from.
		roots.push(".\\");
		roots.push("..\\..\\");
		

		// Possible file paths to check.
		var filePaths = [];
		filePaths.push("modules\\" + fileName);
		filePaths.push("modules\\" + name);
		
		if (!fileType) {
			filePaths.push("modules\\" + name + ".js");
			filePaths.push("modules\\" + name + ".json");
		}
		
		filePaths.push(fileName);
		filePaths.push(shortName + ".js");
		filePaths.push(shortName + ".json");

		var root;
		var fullPath;
		for (var i = 0; i < roots.length; i++) {
			root = roots[i];
			debug("Trying root: " + root);
			
			fullPath = checkRootPath({root:root, filePaths:filePaths, name:name, shortName:shortName, fileName:fileName, fileType:fileType, fso:fso});
			debug("fullPath: " + fullPath);
			
			if (fullPath) {
				return fullPath;
			}
		}
	}

	// Check a root path for a module file/folder.
	function checkRootPath(params) {
		var root        = params.root;
		var filePaths   = params.filePaths;
		var name        = params.name;
		var shortName   = params.shortName;
		var fileName    = params.fileName;
		var fileType    = params.fileType;
		var fso         = params.fso;
		debug("<checkRootPath>", root, name, fileName);
		
		var filePath;
		for (var i = 0; i < filePaths.length; i++) {
			filePath = filePaths[i];
			debug("Checking #" + i, filePath, "exists: " + fso.FileExists(root + filePath));
			
			if (fso.FileExists(root + filePath)) {
				debug(" Found " + name + " @ " + root + filePath);
				return fso.GetFile(root + filePath).Path;
			}
		}

		if (fso.FolderExists(root + "modules\\" + shortName)) {
			debug(" " + root + "modules\\" + shortName + " folder exists!");
			return lookForFolderModule({name:name, fileName:fileName, fileType:fileType, fso:fso, folderPath:root + "modules\\" + shortName});
		} else if (fso.FolderExists(root + "modules\\" + name)) {
			debug(" " + root + "modules\\" + name + " folder exists!");
			return lookForFolderModule({name:name, fileName:fileName, fileType:fileType, fso:fso, folderPath:root + "modules\\" + name});
		}
	}
	
	// Figure out what file to load inside a folder that matches the module name we're looking for. 
	function lookForFolderModule(params) {
		var fso         = params.fso;
		var folderPath  = params.folderPath;
		var fileName    = params.fileName;
		var name        = params.name;
		var fileType    = params.fileType;
		debug("<lookForFolderModule>", folderPath, fileName, name);
	
		var folder = fso.GetFolder(folderPath);
		if (fso.FileExists(folder.Path + "\\" + fileName)) {
			debug(" Found " + name + " @ " + folder.Path + "\\" + fileName);
			return fso.GetFile(folder.Path + "\\" + fileName).Path;
		} else if (!fileType && fso.FileExists(folder.Path + "\\" + fileName + ".js")) {
			debug(" Found " + name + " @ " + folder.Path + "\\" + fileName + ".js");
			return fso.GetFile(folder.Path + "\\" + fileName + ".js").Path;
		} else if (!fileType && fso.FileExists(folder.Path + "\\index.js")) {
			debug(" Found " + name + " @ " + folder.Path + "\\index.js");
			return fso.GetFile(folder.Path + "\\index.js").Path;
		} else if (!fileType && fso.FileExists(folder.Path + "\\" + fileName + ".json")) {
			debug(" Found " + name + " @ " + folder.Path + "\\" + fileName + ".json");
			return fso.GetFile(folder.Path + "\\" + fileName + ".json").Path;
		}
	}
	
	function getContents(filename) {
		// Get the contents of the file.
		//https://raw.githubusercontent.com/jrburke/r.js/wsh/dist/r.js
		//var input_stream = fso.OpenTextFile(file_path, 1, false);
		//var contents = input_stream.ReadAll();
		//input_stream.Close();
		
		var contents, stream = new ActiveXObject("ADODB.Stream");
		stream.Open();
		stream.Type = FileReadTypes.adTypeText;
		stream.Charset = 'utf-8';
		stream.LoadFromFile(filename);
		contents = stream.ReadText(-1);
		stream.Close();
		return contents;
	}
	
	// Main module constructor.
	var Module = function(params) {
		this.name                   = params.name;
		this.exports                = {};
		var parent = this.parent    = params.parent;
		if (parent && parent.children) {
			parent.children.push(this);
		}

		this.loaded             = false;
		this.children           = [];
		this.filename           = params.filename;
	};

	// toJSON data to prevent looping between parent and children modules.
	Module.prototype.toJSON = function (key) {
		debug("<toJSON> ", key);

		return {
			name        : this.name,
			exports     : this.exports,
			parent      : this.parent && this.parent.toString(),
			loaded      : this.loaded,
			children    : this.children,
			filename    : this.filename
		};
	};

	function createGlobal(name, value) {
		debug("<createGlobal> " + name);
		if (name.match(/^[a-zA-Z_$][0-9a-zA-Z_$]*$/)) {
			new Function('value', name + " = value;").call(undefined, value);
		} else {
			//throw new Error("Cannot create global object '" + name + "', invalid JScript ES3 name.");
			outputLine("Cannot create global object '" + name + "', invalid JScript ES3 name.");
		}
	}
	
	Module.prototype.load = function(params) {
		var filename = params.filename;
		debug("<load> ", filename);
		debug("params.dirnameOverride: " + params.dirnameOverride);
		
		// Check if the module is already loaded.
		if (this.loaded == true) {
			throw new Error(this.name + " is already loaded.");
		}
		
		// Assign the filename to the module.
		this.filename = filename;

		// Get the contents of the file.
		var eval_code = getContents(filename) + ";";

		// Get the file object.
		var fso = new ActiveXObject("Scripting.FileSystemObject");
		var file = fso.GetFile(filename);

		// Some modules use 'this' to get the global scope. So lets provide them with a fake environment object.
		// Note, modules that do this cannot be loaded via swx file, will result in a "Object doesn't support this property or method" error.
		// Also this can result in bugs where the module expects certain keys to be in the environment object (like other modules). 
		var env = {};
		
		// Execute the eval string with our variables.
		debug(" evaling " + filename + " (" + this.name + ")...");

		var fakeGlobal = {};
		var fakeProcess = {version:"", env:env};
		
		// __dirname variable that module will see.
		var dirname = params.dirnameOverride || file.ParentFolder.Path;
		debug("Showing __dirname: " + (dirname));
		
		// A require function the module will see. 
		var self = this;
		function load_require(path, params) {
			var parent = params && params.params;
			debug("<load_require>", path, parent);
			params = params || {};
			params.dirnameOverride = params.dirnameOverride || dirname;
			return self.require(path, params);
		}
		
		try {
			new Function('require', 'exports', 'module', '__filename', '__dirname', 'global', 'process', eval_code).call(env, load_require, this.exports, this, this.filename, dirname, fakeGlobal, fakeProcess);
		}catch (e) {
			throw new Error("Error while evaling " + file.Name + ", code: " + (e.number & 0xFFFF) + ", message: " + e.description || e.message);
		}

		// Check if module created a returnExports object in env, if so create a global object of the module's ID name and assign the returnExports to it.
		if (typeof env.returnExports !== "undefined") {
			createGlobal(this.name, env.returnExports);
			this.exports = env.returnExports;
			delete env.returnExports;
		}
		
		// Assign any keys in the environment object as a global object.
		for (var key in env) {
			if (!env.hasOwnProperty(key)) continue;
			createGlobal(key, env[key]);
		}
		
		// Notify if anything was added to fakeGlobal. 
		for (var key in fakeGlobal) {
			if (!fakeGlobal.hasOwnProperty(key)) continue;
			outputLine(this.name + " added '" + key + "' to fakeGlobal!");
		}
		
		// Set the module as loaded.
		this.loaded = true;

		return this;
	};
	
	// Make the toString() more informative.
	Module.prototype.toString = function () {
		return "[Module " + this.name + "]";
	};

	// module.require() function that passes the module's directory path to the real require() for reletive path loading.
	Module.prototype.require = function(path, params) {
		params = params || {};
		debug("<module.require " + this.name + "> " + path + ", parent: " + (params && params.parent) + ", this: " + this);
		debug("module.require params.dirnameOverride: " + (params && params.dirnameOverride));
		params.parent = params.parent || this;
		return require(path, params);
	};

	// don't want outsider redefining "require" and don't want
	// to use arguments.callee so name the function here.
	var require = function(name, params) {
		var parent = params && params.parent;
		var silent = params && typeof params.silent !== "undefined" && params.silent || false;
		
		debug("<require>", name, parent);
		debug("require params.dirnameOverride: " + (params && params.dirnameOverride));
		if (exportsObjects.hasOwnProperty(name)) {
			debug(" returning cached copy.");
			return exportsObjects[name];
		}

		var fso = new ActiveXObject("Scripting.FileSystemObject");
		var dirname;
		if (typeof parent !== "undefined") {
			var parentFileName = parent.filename;
			var parentFile = fso.GetFile(parentFileName);
			dirname = parentFile.ParentFolder.Path;
		}

		debug("<require> name: " + name + ", parent: " + parent + ", dirname: " + dirname);

		var file_path = name;
		
		
		// If a dirname is given, prefix the file path with it. To give the illusion of reletive path for modules that require other modules.
		if (typeof dirname === "string") {
			file_path = dirname + "\\" + file_path;
		}

		if (!name.match(/(\.[^.]+)$/)) {
			// No file type given, try to guess.
			if (fso.FileExists(file_path + ".js")) {
				file_path += ".js";
			} else if (fso.FileExists(file_path + ".json")) {
				file_path += ".json";
			}
		}
		
		if (!fso.FileExists(file_path)) {
			file_path = findModule({
				name           : name, 
				parent         : parent, 
				dirnameOverride: params && params.dirnameOverride
			});
		}

		// Check if the file exists.
		if (!file_path || !fso.FileExists(file_path)) {
			if (silent == false) {
				if (parent) {
					throw new Error("require() cannot find " + name + " reletive to " + parent);
				} else {
					throw new Error("require() cannot find " + name);
				}
			}
			return;
		}

		var file = fso.GetFile(file_path);

		// Check if the file is a json file, if so just return the parsed contents.
		if (file.Type == "JSON File") {
			if (typeof JSON === "undefined") {
				throw new Error("Cannot parse " + file_path + " (JSON is not loaded).");
			}
			var contents = getContents(file.Path);
			return JSON.parse(contents);
		}

		// Create the file's module object.
		var module = new Module({name:name, parent:parent, filePath:file.Path});

		// Load the contents of the file.
		module.load({filename:file.Path, dirnameOverride:(params && params.dirnameOverride)});

		// Cache the exports.
		exportsObjects[name] = module.exports;
		
		//debug("module: " + JSON.stringify(module, undefined, "\t"));

		// Return the exports to the caller.
		return exportsObjects[name];	
	};
	return require;
})();

/*
	//// Module example.
	exports.foo = function() {
		return "bar";
	}
	
	//// Or to be compatible with both swx and require().
	(function (factory) {
		if (typeof module === 'object' && module.exports) {
			module.exports = factory(); // Loaded via require(), returns as exports.
		} else {
			LibFooBar = factory();// Loaded via swx, Global object for your module.
		}
	}(function () {// Where all your code goes.
		function foo() {
			return "bar";
		}
		return { // Public API
			foo    : foo
		};
	}));

	//// Then in your script. You can insure your module is available by adding.
	var _LibFooBar = (typeof LibFooBar !== "undefined" && LibFooBar) || require("LibFooBar");
	
	// Note Skunkworks uses Windows' JScript 5.6+, which is ECMA-262 3rd edition (ES3). ES5 & ES6 shims can be loaded with minor modifications.
	// http://www.jiehuang.net/wordPress/uncategorized/ecmascript/
	// https://github.com/es-shims/
	
	// References.
	// https://gist.github.com/asakura-hunyosi/734296
	// http://wiki.commonjs.org/wiki/Modules/1.1
	// https://darrenderidder.github.io/talks/ModulePatterns/#/
*/